//alert("wdawdawdada");
/*var email = "";
var password = "";*/
let login = document.getElementById("loginbutton");
let emailInput = document.getElementById("emailInput");
let passwordInput = document.getElementById("passwordInput");
let errors = document.getElementById("errors");

const btn_check_fields = (event) => {
    event.preventDefault();

    if (emailInput.value != "" & passwordInput.value != "") {
        console.log(emailInput.value + "\n" + passwordInput.value);

        SendLoginRequest(emailInput.value, passwordInput.value);
    }
    else {
        console.log("Error!!!");
        errors.innerText = "You must fill the fields!";
        errors.classList.add("show");
    }
}

const clear_error = (event) => {
    errors.classList.remove("show");
}

const SendLoginRequest = (email, password) => {
    console.log("send");
    const url = "https://working-domain.ru/api-educ/api/login";
    const config = {
        method: 'post',
        body: JSON.stringify({
            email: email,
            password: password
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };

    fetch(url, config).then(r => r.json()).then(response => console.log(response)).catch(error => { console.log(error); });
}

//console.log(login);
login.addEventListener("click", btn_check_fields);

emailInput.addEventListener("keydown", clearError);

passwordInput.addEventListener("keydown", clearError);