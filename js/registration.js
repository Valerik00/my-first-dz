let registration = document.getElementById("registration");
let nameInput = document.getElementById("nameInput");
let emailInput = document.getElementById("emailInput");
let passwordInput = document.getElementById("passwordInput");
let secondPasswordInput = document.getElementById("secondPasswordInput");
let errors = document.getElementById("errors");

const btn_check_fields = (event) => {
    event.preventDefault();

    if (nameInput.value != "" & emailInput.value != "" & passwordInput.value != "" & secondPasswordInput.value != "") {
        if (passwordInput.value != secondPasswordInput.value) { 
            show_errors("Password mismatch!");
        }
        else {
            console.log(emailInput.value + "\n" + passwordInput.value);

            SendRegistrationRequest(emailInput.value, passwordInput.value, secondPasswordInput.value, nameInput.value);
        }      
    }
    else {
        show_errors("You must fill the fields!");
    }
}

const clearError = () => {
    errors.classList.remove("show");
}

const show_errors = (error) => { 
    errors.innerText = error;
    errors.classList.add("show");
}

const SendRegistrationRequest = (email, password, passwordConfirmation, name) => {
    console.log("send");
    const url = "https://working-domain.ru/api-educ/api/register";
    const config = {
        method: 'post',
        body: JSON.stringify({
            email: email,
            password: password,
            password_confirmation: passwordConfirmation,
            name: name
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    };

    fetch(url, config)
        .then(r => r.json())
        .then(response => {
            if (response.errors != undefined) 
                return Promise.reject(response); 

            console.log(response);
        })
        .catch(error => show_errors(error.errors));
}

registration.addEventListener("click", btn_check_fields);

nameInput.addEventListener("keydown", clearError);

emailInput.addEventListener("keydown", clearError);

passwordInput.addEventListener("keydown", clearError);

secondPasswordInput.addEventListener("keydown", clearError);
secondPasswordInput.addEventListener("keydown", () => { secondPasswordInput.classList.remove("repeat_pas_error"); });